from django.contrib import admin
from hellodjango.helloapp.models import UnknownUser, Author, Source, Type, TagItem, Category, Content, ContentDetail, ContentUser

class ContentAdmin(admin.ModelAdmin):
    list_display = ('title', 'created_at', 'short_description', 'video')
    list_filter = ('created_at', 'title',)
    date_hierarchy = 'created_at'
    ordering = ('created_at',)
    prepopulated_fields = {"slug" : ("title",)}
    

class AuthorAdmin(admin.ModelAdmin):
    list_display =('last_name','first_name',)
    search_field =('last_name',)
    prepopulated_fields = {"slug" : ("last_name", "first_name",)}

class SourceAdmin(admin.ModelAdmin) :
    list_display=('source',)
    prepopulated_fields = {"slug" : ("source",)}

class TypeAdmin(admin.ModelAdmin) :
    list_display=('description',)
    prepopulated_fields = {"slug" : ("description",)}


class CategoryAdmin(admin.ModelAdmin) :
    list_display = ('name',)
    prepopulated_fields = {"slug" : ("name",)}
    fields = ('name', 'slug',)

class ContentUserAdmin(admin.ModelAdmin) :
    list_display = ('my_list_choices',)
    prepopulated_fields = {"slug" : ("title",)}

    
admin.site.register(UnknownUser)
admin.site.register(Author, AuthorAdmin)
admin.site.register(Source, SourceAdmin)
admin.site.register(Type, TypeAdmin)
admin.site.register(TagItem)
admin.site.register(Category, CategoryAdmin)
admin.site.register(Content, ContentAdmin)
admin.site.register(ContentDetail)
admin.site.register(ContentUser, ContentUserAdmin)